import json
import streamlit as st
import pandas as pd
import time

response_ai_json = '{"trade": "yes","if": "yes","af": "yes","overdraft": "yes"}'
af_response_data = '{"amount": "£300,000","days": "90","productname": "Asset Finance","industrypeers": "50%"}'
trade_response_data = '{"amount": "50000","days": "180","productname": "trade","industrypeers": "75%"}'
if_response_data = '{"amount": "80000","days": "130","productname": "if","industrypeers": "85%"}'
overdraft_response_data = '{"amount": "80000","days": "130","productname": "if","industrypeers": "85%"}'


# tab1, tab2 = st.tabs(["Customer","RM"])

# with tab1:

st.header("RM")
input=json.loads(response_ai_json)


if input["af"] == 'yes':
    message_details=json.loads(af_response_data)
    st.toast(f'Good Afternoon Mary, for your meeting with :blue[Dan EV ltd.] next week ...',icon='🔌')
    time.sleep(6)
    st.toast(f'Did you know they have a preassesed limit of :green[{message_details['amount']}]?',icon='💷') 
    time.sleep(6)
    st.toast(f'Did you know :blue[{message_details['industrypeers']}] of their peers use {message_details['productname']} to help their business?',icon='📈')
    time.sleep(6)
    st.toast(f'John, our Asset Finanancing specialist, is available for this client meeting',icon='🤝')
    time.sleep(6)
    st.toast(f'I have prepared a draft summary you can send to John with the invite. Check outlook drafts 😉',icon='📧')
    time.sleep(6)


# if input["trade"] == 'yes':
#     message_details=json.loads(trade_response_data)
#     st.toast(f'Hi Alex, Did you know you have {message_details['amount']} in your account for {message_details['days']}. Have you heard of {message_details['productname']}. {message_details['industrypeers']} of your peers currently use this product. More info click Here',icon='😋')
#     time.sleep(.5)

# if input["if"] == 'yes':
#     message_details=json.loads(if_response_data)
#     st.toast(f'Hi Mary, Did you know you have {message_details['amount']} in your account for {message_details['days']}. Have you heard of {message_details['productname']}. {message_details['industrypeers']} of your peers currently use this product. More info click Here',icon='😋')
#     time.sleep(.5)

# if input["af"] == 'yes':
#     message_details=json.loads(af_response_data)
#     st.toast(f'Hi James, Did you know you have {message_details['amount']} in your account for {message_details['days']}. Have you heard of {message_details['productname']}. {message_details['industrypeers']} of your peers currently use this product. More info click Here',icon='🥚')
#     time.sleep(.5)

# with tab2:
#     st.header("RM")
#     st.write("Hello RM")

    # listCompanyNames = ['Tesco','Name2','Name3']
    # listProducts = [{'dProductName','Product Name'},{'dProductDescription','Product Descriptionn'}]

    # with st.expander('Products we think you may like, exapand for more info...'):
    #     row1 = st.container()
    #     with row1:
    #         st.write('Suggested products')
    #         dfProducts = pd.DataFrame(listProducts)
    #         st.table(dfProducts)

    # st.empty()
    # row2 = st.container()
    # with row2:
    #     with st.form(key='formCompanyDetails'):
    #         strCompanyName = st.selectbox('company name',listCompanyNames)
    #         btnSubmit = st.form_submit_button(label='Find details')

    # if btnSubmit:
    #     r3col1,r3col2 = st.columns([1,3])
    #     with r3col1:
    #         st.write('Cusomter Name')
    #     with r3col2:
    #         st.text(strCompanyName)
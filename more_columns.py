import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor
import csv


# Initialize an empty list to store data
data = []

# Create 500 rows of times tables
def training_data():
    for _ in range(500):
        # Choose a random times table (from 1 to 10)
        times_table = np.random.randint(1, 11)
        
        # Generate the times table data (1 through 10)
        times_table_data = [i * times_table for i in range(1, 11)]
        
        # Append to the main data list
        data.append(times_table_data)

    # Convert the list of lists into a pandas DataFrame
    df = pd.DataFrame(data)

    # Display the DataFrame (optional)
    return df

# Create training data (this is a dataframe of times tables)
df_train = training_data()
print("df_train: ", df_train)

# Create test data (this is a dataframe of times tables, we want later the model to predict the last column)
data_test = {0: [2, 4, 5], 1: [4, 8, 10], 2: [6, 12, 15], 3: [8, 16, 20], 4: [10, 20, 25], 5: [12, 24, 30], 6: [14, 28, 35], 7: [16, 32, 40], 8: [None, None, None], 9: [None, None, None]}
df_test = pd.DataFrame(data_test)

X_train = df_train[[0, 1, 2, 3, 4, 5, 6, 7]]
y_train = df_train[[8, 9]]

model = RandomForestRegressor()
# Training the ML model with the training data (the data is complete)
model.fit(X_train, y_train)

X_test = df_test[[0, 1, 2, 3, 4, 5, 6, 7]]
# Model uses historical data to predict the last column
df_test[[8, 9]] = model.predict(X_test).astype(int)

# Display the completed dataset
print("AI generated results: ", df_test)
